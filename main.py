# -*- coding: utf-8 -*-
from pyaudio import PyAudio, paInt16
from aip import AipSpeech
import numpy as nu
import wave
import json
import sys
import os

reload(sys)
sys.setdefaultencoding("utf-8")

# Save Wave File
def save_wave_file(filename, data):
    wf = wave.open(filename, 'wb')
    wf.setnchannels(1)
    wf.setsampwidth(2)
    wf.setframerate(SAMPLING_RATE)
    wf.writeframes("".join(data))
    wf.close()    

# Read File
def get_file_content(filePath):
    with open(filePath, 'rb') as fp:
        return fp.read()

# Speech Rectongition
APP_ID = "9856167"
APP_KEY = "cB6CFsT0dC5k28csPT91Ff6g"
SECRECT_KEY = "7a34eeebb659efeb5dbda2169c14545f" 

aipSpeech = AipSpeech(APP_ID, APP_KEY, SECRECT_KEY)

NUM_SAMPLES = 8000     # pyAudio内部缓存的块的大小
SAMPLING_RATE = 48000   # 取样频率
LEVEL = 1500            # 声音保存的阈值
COUNT_NUM = 150          # NUM_SAMPLES个取样之内出现COUNT_NUM个大于LEVEL的取样则记录声音

pyAudio = PyAudio()
audio_stream = pyAudio.open(format=paInt16, channels=1, rate=SAMPLING_RATE, input=True, frames_per_buffer=NUM_SAMPLES)

isRecording = False
speech_buffer = []
speech_buffer_len_min = 3           # 有效buff的长度大于一定数值,才是有效的录音
invalid_speech_buffer_count = 0     # 录音时,积累的无效buff次数
invalid_speech_buffer_count_max = 3 # 无效次数达到一直数值,停止录音

print "等待语音输入"
try:
    while True:
        valid_num_frames = audio_stream.get_read_available()

        # print "当前%d"%(valid_num_frames)

        string_audio_data = audio_stream.read(NUM_SAMPLES, exception_on_overflow = False)
        audio_data = nu.fromstring(string_audio_data, dtype = nu.short)    
        sample_max_value = nu.max(audio_data)

        # print  "采样最大值: %d" % (sample_max_value)

        if isRecording == False:
            valid_sample_count = nu.sum( audio_data > LEVEL)
            if valid_sample_count > COUNT_NUM:
                print "开始录音,有效采样计数 %s" % (valid_sample_count)
                isRecording = True
                invalid_speech_buffer_count = 0
        
        if isRecording:        
            if sample_max_value > LEVEL:
                speech_buffer.append(string_audio_data)
                invalid_speech_buffer_count = 0
            
            invalid_speech_buffer_count += 1
        
            if invalid_speech_buffer_count >= invalid_speech_buffer_count_max:
                
                speech_buffer_count = len(speech_buffer)

                print "语音buff  长度%d"%(speech_buffer_count)

                if speech_buffer_count >= speech_buffer_len_min:

                    # 保存文件
                    save_wave_file("speechRaw.wav", speech_buffer)                
                    
                    # 转换采样频率                    
                    os.system('sox speechRaw.wav -r 8000 speech.wav')

                    print "开始识别"
                    # 识别语音
                    voice_data = aipSpeech.asr(get_file_content('speech.wav'), 'wav', 8000, {'lan': 'zh'})
                    if voice_data["err_no"] == 0:
                        print "输入语音: %s" %(voice_data['result'][0])
                    else:
                        print "无法识别"
                    print "等待语音输入"
                
                # 重置状态
                print "重置状态"
                isRecording = False
                speech_buffer = []
                invalid_speech_buffer_count = 0
except KeyboardInterrupt:
		audio_stream.close()